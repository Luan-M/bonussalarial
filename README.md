Esse bônus salarial vai depender se a empresa atingiu a meta ou não.
Se a empresa atingir a meta o funcionário vai receber o salário e mais 100% do salário dele.
Se a empresa faturar 80% da meta, o funcionário vai receber o salário e mais 80% do salário dele.
Se a empresa não atingir nem 80% da meta, o funcionário vai receber só o salário de normal.
