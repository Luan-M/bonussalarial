package domain;

import java.util.Scanner;

public class BonusSalarial {
	
	static final Double PORCENTAGEM = 0.8;
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Digite a meta da sua empresa: "); 
		Double metaEmpresa = scanner.nextDouble();
		
		System.out.print("Digite o faturamento da empresa: ");
		Double faturamentoEmpresa = scanner.nextDouble();
		
		System.out.print("Digite o salário do funcionário: ");
		Double salarioFuncionario = scanner.nextDouble();
		
		Double oitentaPorcentoMeta = metaEmpresa * PORCENTAGEM;
		Double oitentaPorcentoSalario = salarioFuncionario * PORCENTAGEM;
		
		Boolean cemPorcentoBonus = faturamentoEmpresa >= metaEmpresa;
		Boolean oitentaPorcentoBonus = faturamentoEmpresa >= oitentaPorcentoMeta;
		
		System.out.println();
		
		if(cemPorcentoBonus) {
			salarioFuncionario += salarioFuncionario;
			System.out.println("Bônus de 100% do salário para o funcionário.");
			System.out.println("Salário: " + salarioFuncionario);
			
		} else if(oitentaPorcentoBonus) {
			salarioFuncionario += oitentaPorcentoSalario;
			System.out.println("Bônus de 80% do salário para o funcionário.");
			System.out.println("Salário: " + salarioFuncionario);
			
		} else {
			System.out.println("Como a empresa não atingiu a meta, o funcionário terá 0% de bônus no salário.");
			System.out.println("Salário: " + salarioFuncionario);
		}
		
		scanner.close();
	}

}
